/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * napi 声明文件
 */
import { Callback } from './basic';
declare namespace parkingNative {
    // 响应值
    interface Response {
        /**
         * 响应状态
         */
        state: ResponseState;

        /**
         * 设备状态
         */
        result: string;

        /**
         * 响应时间
         */
        time?:string
    }
    export enum ResponseState {
        /**
         * 控制命令响应状态值.
         */
        SUCCESS = 200,
        ERROR = 500
    }

    // 设备状态值
    type deviceState = 'online' | 'offline';
    // 车辆状态值
    type carState = 'waiting' | 'leave'| 'near'; // 车辆等待中 已离开  靠近中
    //
    type latchOperate = 'open' | 'close'; // 车辆等待中 已离开  靠近中
    // 设备数据
    interface DeviceInfo {
        /**
         * 设备名
         */
        name: string;

        /**
         * 设备类型 避免和关键字type冲突
         */
        deviceType: string;

        /**
         * 设备id
         */
        id: string;

        /**
         * 设备状态
         */
        state: deviceState;

        /**
         * 分组 取值类似 A_ENTRANCE B_EXIT  A/B/C/D + _ + ENTRANCE/EXIT
         */
        group: string;

        /**
         * 设备信息 各个设备视情况保存不同的数据
         */
         deviceData?:object;
    }

    // 车辆信息
    interface CarInfo {
        /**
         * 车牌
         */
        licensePlate: string;

        /**
         * 设备状态
         */
        state: carState;
    }

    // 初始化 让设备去L2 去发广播
    function init(): Promise<Response>;
    // 打开门禁
    function controlLatch(id:string,operate:latchOperate): Promise<Response>;
    // 获取设备列表 只返回一次结果，不同于on中不断的触发回调
    function getDeviceList(): Promise<Array<DeviceInfo>>;
    /**
     * 监听是否有车辆进入（1. 来自摄像头云端识别 2.来自自己本地识别）
     * 触发回调方式：
     * 车辆来了，有摄像头则获取到车牌识别结果后进入回调（携带识别结果），无则感应到车辆靠近则触发（无识别结果）
     * 车辆离开触发回调
     * @param type carAccess
     * @param callback 回调
     */
    function on(type: 'carAccess', callback: Callback<CarInfo>): void;
    /**
     * 订阅所有设备设备更新状态
     * @param type devicesUpdate
     * @param callback 回调
     * @param intervalTime 可选 更新列表间隔时间 默认值 3s
     */
    function on(type: 'devicesUpdate', callback: Callback<Array<DeviceInfo>>,intervalTime:?number): void;
    // 取消车辆出入监听
    function off(type: 'carAccess', callback?: Callback<Response>): void;
    // 取消所有设备设备更新状态监听
    function off(type: 'devicesUpdate', callback?: Callback<Response>): void;
    // 释放napi下各个线程 调用后意味着设备下线
    function release(): Promise<Response>;
}

export default parkingNative;