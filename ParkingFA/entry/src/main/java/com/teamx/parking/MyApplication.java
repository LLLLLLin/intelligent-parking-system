package com.teamx.parking;

import com.socks.library.KLog;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        KLog.init(true, "Parking_log");
        super.onInitialize();
    }
}
