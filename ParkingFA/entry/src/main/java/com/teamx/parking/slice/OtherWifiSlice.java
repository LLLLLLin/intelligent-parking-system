/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teamx.parking.slice;

import com.socks.library.KLog;
import com.teamx.parking.ResourceTable;
import com.teamx.parking.bean.WifiList;
import com.teamx.parking.provider.WifiListProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiScanInfo;

import java.util.ArrayList;
import java.util.List;

public class OtherWifiSlice extends AbilitySlice {
    private final List<String> wifiList = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_other_wifi);
        initComponents();
        getAvailableWifi();
        initListContainer();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * 获取其他可连接的wifi
     *
     */
    private void getAvailableWifi() {
        WifiDevice wifiDevice = WifiDevice.getInstance(getContext());
        // 调用WLAN扫描接口
        boolean isScanSuccess = wifiDevice.scan();
        if (!isScanSuccess) {
            //获取wifi列表失败失败
            return;
        }

        // 调用接口获取扫描结果
        List<WifiScanInfo> scanInfos = wifiDevice.getScanInfoList();

        // 以BSSID为维度，因此需要ssid来删除重复
        for (WifiScanInfo scanInfo : scanInfos) {
            if (scanInfo.getSsid() == null || scanInfo.getSsid().trim().equals("")
                    || wifiList.contains(scanInfo.getSsid())) {
                continue;
            }
            wifiList.add(scanInfo.getSsid());
        }
    }

    private void initListContainer() {
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_results_container);
        List<WifiList> list = getData();
        WifiListProvider wifiListProvider = new WifiListProvider(list, this);
        listContainer.setItemProvider(wifiListProvider);

        listContainer.setItemClickedListener((iListContainer, component, position, id) -> {
            WifiList item = (WifiList) listContainer.getItemProvider().getItem(position);
            setNettingName(item.getName());
        });
    }

    private ArrayList<WifiList> getData() {
        ArrayList<WifiList> list = new ArrayList<>();
        for (String view : wifiList) {
            list.add(new WifiList(view));
        }
        return list;
    }

    @Override
    protected void onBackPressed() {
        setNettingName("");
    }

    private void setNettingName(String wifiSsid) {
        KLog.i("setNettingName wifiSsid==" + wifiSsid);
        Intent intent = new Intent();
        intent.setParam("ssid", wifiSsid);
        setResult(intent);
        terminate();
    }
    private void initComponents() {
        findComponentById(ResourceTable.Id_otherWifi_text_cancel).setClickedListener(component -> {
            terminate();
        });
    }
}
