/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teamx.parking.slice;

import cn.bertsir.zbar.utils.QRUtils;
import com.teamx.parking.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.media.image.PixelMap;


public class QRCodeSlice extends AbilitySlice {
    private String ssid;
    private String password;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_qr_code);
        ssid = intent.getStringParam("ssid");
        password = intent.getStringParam("password");
        initComponents();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initComponents() {
        PixelMap qrCode = QRUtils.getInstance().createQRCode(String.format("ssid:%s;password:%s;group:A_ENTRANCE;",ssid,password),500,500);
        Image qrcodeImg = (Image) findComponentById(ResourceTable.Id_qrcode_img_qrcode);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(AttrHelper.vp2px(16,getContext()));
        qrcodeImg.setPixelMap(qrCode);
        qrcodeImg.setBackground(shapeElement);

    }
}
