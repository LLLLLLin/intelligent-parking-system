/*************************************************************************
    > File Name: dlist.h
    > Version: 1.0
    > Author: 蔡奇
    > Mail: caiqi1126@163.com 
    > Created Time: 2022年02月18日 星期五 15时21分49秒
 ************************************************************************/

#ifndef LOCAL_NET_DLIST_H
#define LOCAL_NET_DLIST_H

#include<stdio.h>
#include "local_net_def.h"

typedef struct  Node                //链表节点
{
    struct  Node    *before;    //前向指针
    void *data;        //数据指针　
    struct  Node    *after;    //后向指针
} Node_t;

/****************
 * 功能：创建双向链表
 * 参数：无
 * 返回值：指向该被创建的双向链表头节点的指针
 * *************/
 Node_t *CreateDlist();

/****************
 * 功能：链表头部插入数据
 * 参数：head:链表头部指针， newvalue:指向该新添加数据的指针， size:该新添加数据的大小
 * 返回值：0添加失败 1添加成功
 * *************/
int InsertHdlist( Node_t *head,const void *newdata,int size);    

/****************
 * 功能：链表尾部插入数据
 * 参数：head:链表头部指针， newvalue:指向该新添加数据的指针， size:该新添加数据的大小
 * 返回值：0添加失败 1添加成功
 * *************/
int InsertTdlist( Node_t *head,const void *newdata,int size);    

/****************
 * 功能：指向比较函数的函数指针
 * 参数：data1：指向第一个数据的指针，data2：指向第二个数据的指针
 * 返回值：大于0：data1大于data2, 小于0：与前面相反， 等于0：data1和data2相等
 ***************/
typedef int (*CmpFun_t)(const void* data1, const void* data2);

/****************
 * 功能：按值查找数据所在链表的节点
 * 参数：head链表头部指针， value:目标节点所包含的数据， cmpFun:指向比较函数指针
 * 返回值：指向目标数据所在节点的指针
 * *************/
 Node_t *FindVdlist(Node_t *head,const void *value,CmpFun_t cmpFun);

/****************
 * 功能：选择排序该链表
 * 参数：head:链表头部指针，cmpFun:比较函数指针
 * 返回值：无
 * *************/
void SelectSortdlist(Node_t *head,CmpFun_t cmpFun);

/****************
 * 功能：指向打印函数的函数指针
 * 参数：data：指向节点数据的指针
 * 返回值：无
 ***************/
typedef void (*PrintNodeFun_t)(const void* data);

/****************
 * 功能：打印所在链表的所有节点数据
 * 参数：head链表头部指针， PrintNodeFun_t:指向节点打印函数指针
 * 返回值：无
 * *************/
void Printdlist(Node_t *head, PrintNodeFun_t printFun);

/****************
 * 功能：在链表头部删除一个节点
 * 参数：head:链表头部指针
 * 返回值：0删除失败，1删除成功
 * *************/
int DeleteHdlist( Node_t *head);

/****************
 * 功能：在链表尾部删除一个节点
 * 参数：head:链表头部指针
 * 返回值：0删除失败，1删除成功
 * *************/
int DeleteTdlist( Node_t *head);

/****************
 * 功能：按值删除链表中的节点
 * 参数：head:链表头部指针， value:被删除节点所包含的数据， cmpFun:比较函数指针
 * 返回值：该链表头节点指针
 * *************/
Node_t *DeleteVdlist( Node_t *head,const void *value,CmpFun_t cmpFun);    

/****************
 * 功能：删除链表
 * 参数: head指向该链表头节点的二级指针
 * 返回值：1：删除成功，0:删除失败
 * *************/
int DestroyDlist( Node_t **head);

#endif  // LOCAL_NET_DLIST_H
