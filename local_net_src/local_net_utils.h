/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __LOCAL_NET_UTILS_H__
#define __LOCAL_NET_UTILS_H__

/**
 * @brief 计算字符串SHA-256
 * 
 * @param str 字符串指针
 * @param length 字符串长度
 * @param sha256 用于保存SHA-256的字符串指针
 * @return int 成功返回0
 */
int  StrSha256(char* str, long length, char* sha256);

#endif // __LOCAL_NET_UTILS_H__