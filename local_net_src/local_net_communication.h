/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __LOCAL_NET_COMMUNICATION_H__
#define __LOCAL_NET_COMMUNICATION_H__

#include <time.h>
#include <sys/time.h>
#ifdef L0_DEVICE
#include "cmsis_os2.h"
#endif

#include "local_net_dlist.h"
#include "local_net_udp.h"
#include "local_net_def.h"
#include "cJSON.h"

// 设备在线状态值.
typedef enum {
    LOCAL_NET_ONLINE = 0,
    LOCAL_NET_OFFLINE
} DeviceNetState;

// 广播发送数据结构
typedef struct NetBroadcastPara {
    char name[32];
    char type[32];
    char id[65];
    char group[32];
    uint32_t priority;
    Node_t *subscribe;
    Node_t *publish;
    struct timeval time;
    DeviceNetState state;
} NetBroadcastPara_t;

// 单播发送的数据结构
typedef struct NetUnicastPara {
	char publish[32];
    char params[256];
} NetUnicastPara_t;

// 响应结构
typedef struct ResponsePara {
    char id[DEVICE_ID_LEN];
    /**
     * 设备状态
     */
	char result[32];

	/**
     * 响应状态
     */
    enum ResponseState state;
} ResponsePara_t;

/**
 * @brief 比较str和对象的Id的函数
 * 
 * @param data1 id 的str
 * @param data2 包含id的对象
 * @return int 大于0：data1大于data2, 小于0：与前面相反， 等于0：data1和data2相等
 */

/**
 * 功能：比较Id的函数
 * 参数：data1：指向第一个str的指针，data2：指向第二个str的指针
 * 返回值：大于0：data1大于data2, 小于0：与前面相反， 等于0：data1和data2相等
*/

/*
 * LocalNetSelfInfoSet
 */
int8_t LocalNetSelfInfoSet(NetBroadcastPara_t *selfInfo);

/*
 * LocalNetInit
 */
int8_t LocalNetInit(void);

/*
 * LocalNetDeinit
 */
int8_t LocalNetDeinit(void);

/*
 * LocalNetMsgSend
 */
int8_t LocalNetMsgSend(const char *publish, const char *params);

/*
 * LocalNetMsgRecvCbReg
 */
int8_t LocalNetMsgRecvCbReg(int8_t (*recvCb)(const char *msg));

/*
 * LocalNetDevAttributeBoardCast
 */
int8_t LocalNetDevAttributeBoardCast();

#endif // __LOCAL_NET_COMMUNICATION_H__