/*************************************************************************
    > File Name: comm.h
    > Version: 1.0
    > Author: 蔡奇
    > Mail: caiqi1126@163.com 
    > Created Time: 2019年04月19日 星期五 15时29分24秒
 ************************************************************************/

#ifndef LOCAL_NET_DEF_H
#define LOCAL_NET_DEF_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>

#define DEVICE_ID_LEN 65
#define MQ_MSG_SIZE_MAX 4
#define MQ_MSG_NUM_MAX 10

#define L0_DEVICE 1

#include "log.h"
#include "hilog/hiview_log.h"

#undef LOG_TAG
#define LOG_TAG "LOCAL_NET"

#define LOG_D(fmt, ...) HILOG_DEBUG(HILOG_MODULE_APP, "[%s] %d: "fmt"\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_W(fmt, ...) HILOG_WARN(HILOG_MODULE_APP, "[%s] %d: "fmt"\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_I(fmt, ...) HILOG_INFO(HILOG_MODULE_APP, "[%s] %d: "fmt"\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_E(fmt, ...) HILOG_ERROR(HILOG_MODULE_APP, "[%s] %d: "fmt"\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                             \
    do {                                                                             \
        if (!(cond)) {                                                               \
            HILOG_ERROR("%s:%d '%s' assert failed.\r\n", __FILE__, __LINE__, #cond); \
            while (1)                                                                \
                ;                                                                    \
        }                                                                            \
    } while (0)


enum ResponseState 
{
    /**
     * 控制命令响应状态值.
     */
    SUCCESS = 200,
    ERROR = 500
};

/****************
 * 功能：显示程序异常所在位置、内容的宏
 * 参数：x(待比较的第一个参数), option(比较符号如：> =), y(待比较的第二个参数)，message(自定义的异常内容)， z(自定义的返回值)
 * 返回值: z ：NULL 表示数据为空，-1表示其他异常
 * *************/
#define SYSERR(x,option,y,message,z)	if((x) option (y))\
{\
	printf("\t%s %s %d : error: %s\n", __FILE__, __func__, __LINE__, message);\
	return z;\
}

typedef enum {
    LOCAL_NET_THREAD_INIT = 0,
    LOCAL_NET_THREAD_RUNNING,
    LOCAL_NET_THREAD_RELEASE,
    LOCAL_NET_THREAD_EXIT,
    LOCAL_NET_THREAD_ERR
} LocalNetThreadStatus;

#endif
